import https from 'https';
import http from 'http';
import fs from 'fs';
import { get_url, get_opts } from '../utils/get-options.mjs';


export async function get_template(url_or_path, insecure, login) {

    const source_url = await get_url(url_or_path);

    const opts = await get_opts(insecure, login);

    const content =
        (source_url.protocol === "file:")
            ? await from_file(source_url.pathname)
            : await from_url(source_url, opts);

    return [source_url, JSON.parse(content), opts];
}

async function from_file(filepath) {
    return await fs.promises.readFile(filepath, 'utf8');
}


async function from_url(url, options, postData = null) {
    const request = (url.protocol === "https:") ? https.request : http.request;
    return new Promise((resolve, reject) => {
        const req = request(url, options, (res) => {
            let data = '';
            if (res.statusCode !== 200) {
                res.resume();
                reject(new Error(`${res.statusCode} - ${res.statusMessage}`))
            }
            res.on('data', (chunk) => {
                data += chunk;
            });

            res.on('end', () => {
                resolve(data);
            });
        });

        req.on('error', (error) => {
            reject(error);
        });

        if (postData) {
            req.write(postData);
        }

        req.end();
    });
}
