/*
Template example
{
    "type": "remote-gedi",
    "ca_install": true,
    "project": "https://cd.prod.internal",
    "git_origin_main": "https://ci.develop.internal/proj.git",
    "git_prod_main": "https://cd.prod.internal/proj.git",
    "npm_scope": "@subproj",
    "npm_registry": "https://ci.develop.internal/npm/",
    "npm_prod_registry": "https://cd.prod.internal/npm/"
}

WIP WIP WIP

*/

import { get_template } from './get-template.mjs';
import remote_gedi from '../remote-gedi/create-remote-gedi.mjs';

export async function create_from_template(template_url, insecure, login) {

    const [src, tpl, opts] = await get_template(template_url, insecure, login);
    const tpl_type = tpl.type ?? "";
    if (tpl_type === "remote-gedi") {
        await remote_gedi.create_from_template(src, tpl, opts);
    }
    else {
        throw new Error("template type not found");
    }
}
