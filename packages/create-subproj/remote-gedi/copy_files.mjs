import fs from 'fs';
import path from 'path';

export async function copy_files(sourceDirectory, filePaths, targetDirectory) {
    for (const filePath of filePaths) {
        const sourcePath = path.join(sourceDirectory, filePath);
        const targetPath = path.join(targetDirectory, path.basename(filePath));
        await fs.promises.copyFile(sourcePath, targetPath);
        console.log(`Copied: ${sourcePath} -> ${targetPath}`);
    }
};
