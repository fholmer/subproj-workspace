import path from 'path';
import { download_file } from './download_file.mjs';


export async function download_files(base_url, urls, options, downloadDirectory) {
  try {
    const downloadPromises = urls.map((url, index) => {
      const fileName = path.basename(url);
      const filePath = path.join(downloadDirectory, fileName);
      return download_file(new URL(url, base_url), options, filePath);
    });

    await Promise.all(downloadPromises);
  } catch (err) {
    if (err.code === 'UNABLE_TO_GET_ISSUER_CERT_LOCALLY') {
      throw new Error("Download Error: unable to get local issuer certificate. Retry with the --insecure flag or set NODE_TLS_REJECT_UNAUTHORIZED=0");
    }
    else if (err.code === 'DEPTH_ZERO_SELF_SIGNED_CERT') {
      throw new Error("Download Error: unable to get local issuer certificate. Retry with the --insecure flag or set NODE_TLS_REJECT_UNAUTHORIZED=0");
    }
    else if (err.message === '401') {
      throw new Error("Download Error: 401 Not authenticated. Check user/pass");
    }
    else if (err.message === '502') {
      throw new Error("Download Error: 502 Bad gateway, Project not running?");
    }
    else {
      throw new Error(`Download error: ${err}`);
    }
  }
}
