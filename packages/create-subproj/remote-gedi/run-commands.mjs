/*
Template example
{
    "type": "remote-gedi",
    "ca_install": true,
    "project": "https://cd.prod.internal",
    "git_origin_main": "https://ci.develop.internal/proj.git",
    "git_prod_main": "https://cd.prod.internal/proj.git",
    "npm_scope": "@subproj",
    "npm_registry": "https://ci.develop.internal/npm/",
    "npm_prod_registry": "https://cd.prod.internal/npm/"
}

WIP WIP WIP
*/

import fs from 'fs';
import path from 'path';
import { exec } from 'child_process';
import { promisify } from 'util';

async function validate_template(data, insecure) {
    const tpl = JSON.parse(data);
    const cwd = process.cwd();

    var opts = await interact_with_user(tpl, cwd);

    if (insecure) {
        opts["rejectUnauthorized"] = false;
    }
    process_template(tpl, cwd);
}

async function process_template(tpl, cwd) {

    if (!fs.existsSync(path.join(cwd, ".git"))) {
        await runCmd("git:origin/main:", "git", ["init"]);
    }

    const opts = { encoding: "utf8", shell: true }
    if (tpl.git_origin_main ?? null) {
        await runCmd("git:origin/main:", "git", ["remote", "add", "origin", tpl.git_origin_main]);
    }
    if (tpl.git_prod_main ?? null) {
        await runCmd("git:prod/main:", "git", ["remote", "add", "prod", tpl.git_prod_main]);
    }
    if (tpl.npm_scope ?? null) {
        if (tpl.npm_registry ?? null) {
            await runCmd("npm:registry:", "npm", ["set", `${tpl.npm_scope}:registry=${tpl.npm_registry}`]);
        }
        if (tpl.npm_prod_registry ?? null) {
            await runCmd("npm:prod:registry", "npm", ["set", `${tpl.npm_scope}:prod:registry=${tpl.npm_prod_registry}`]);
        }
        //await runCmd("npm install subproj", "npm", ["install", "subproj"]);
        //runCmd(`npx subproj init ${tpl.npm_scope}`, "npx", ["subproj", "init", tpl.npm_scope]);
    }

    console.info("Done");
}

const proc_opt = {
    windowsHide: true,
    encoding: "utf8",
    shell: true
}

async function runCmd(desc, cmd, args) {
    const ex = promisify(exec);
    const command = cmd + " " + args.join(" ");
    const { stdout, stderr } = await ex(command, proc_opt);
    console.log(desc, stdout, stderr);
}
