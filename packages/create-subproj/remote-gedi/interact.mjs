import readline from 'readline';


export async function interact_with_user(install_path, url, opts) {

    const rl = readline.promises.createInterface({
        input: process.stdin,
        output: process.stdout,
        terminal: true,
    });

    try {
        const accept = await rl.question(`Remote GEDI setup: 

Get config from:    ${url}
Install to path:    ${install_path}

Following files will be installed/replaced:
    host-cert.pem, host-key.pem, root-cert.pem
    config, progs

Accept these changes? (Y/n) `);
        if (accept === "Y" || accept === "y" || accept === "") {
        }
        else {
            throw new Error("Canceled by user");
        }

        if (url.protocol === 'file:') {
            return opts;
        }

        if (opts.headers) {
            return opts;
        }

        const username = await rl.question(`Login with username: `);

        //hide pass
        rl.input.on("keypress", function (c, k) {
            var len = rl.line.length;
            readline.moveCursor(rl.output, -len, 0);
            readline.clearLine(rl.output, 1);
            for (var i = 0; i < len; i++) {
                rl.output.write("*");
            }
        });

        const userpass = await rl.question("Password: ");

        if (username === "" && userpass === "") {
            return opts;
        }

        const auth = Buffer.from(`${username}:${userpass}`).toString('base64');
        return {
            ...opts,
            headers: {
                'Authorization': `Basic ${auth}`
            }
        }
    } catch (err) {
        throw err;
    } finally {
        rl.close();
    }
}




/*
export async function interact_with_user(tpl, install_path) {
    const rl = readline.promises.createInterface({
        input: process.stdin,
        output: process.stdout,
        terminal: true,
    });

    try {
        const accept = await rl.question(`Remote GEDI setup: 

Template type:         ${tpl.type}
Project download url:  ${tpl.project}

Local:
    install path:      ${install_path}
    node_modules:
        - subproj        @latest
Configuration:
    git:origin/main:   ${tpl.git_origin_main}
    git:prod/main:     ${tpl.git_prod_main}
    npm:scope:         ${tpl.npm_scope}
    npm:registry:      ${tpl.npm_registry}
    npm:prod:registry: ${tpl.npm_prod_registry}

Accept these changes? (Y/n) `);
        if (accept === "Y" || accept === "y" || accept === "") {
        }
        else {
            throw new Error("Canceled by user");
        }

        const username = await rl.question(`Login with username: `);

        //hide pass
        rl.input.on("keypress", function (c, k) {
            var len = rl.line.length;
            readline.moveCursor(rl.output, -len, 0);
            readline.clearLine(rl.output, 1);
            for (var i = 0; i < len; i++) {
                rl.output.write("*");
            }
        });

        const userpass = await rl.question("Password: ");

        if (username === "" && userpass === "") {
            return {}
        }

        const auth = Buffer.from(`${username}:${userpass}`).toString('base64');
        return {
            headers: {
                'Authorization': `Basic ${auth}`
            }
        }
    } catch (err) {
        throw err;
    } finally {
        rl.close();
    }
}
*/