import fs from 'fs';
import path from 'path';
import { download_files } from './download_files.mjs';
import { interact_with_user } from './interact.mjs';
import { copy_files } from './copy_files.mjs';
import { get_url, get_opts } from '../utils/get-options.mjs';

const proc_opt = {
  windowsHide: true
}

export default {
  create_from_template,
  create_remote_gedi
}

export async function create_from_template(tpl_src, tpl, opts) {

  const url_or_path = tpl.project;
  const source_url = await get_url(url_or_path);

  await create(source_url, opts);
}

export async function create_remote_gedi(url_or_path, insecure) {

  const source_url = await get_url(url_or_path);
  const opts = await get_opts(insecure, false);

  await create(source_url, opts);
}

async function create(source_url, opts) {

  const cwd = process.cwd();

  opts = await interact_with_user(cwd, source_url, opts);

  const urls = [
    `/config/config`,
    `/config/host-cert.pem`,
    `/config/host-key.pem`,
    `/config/root-cert.pem`,
  ];

  const download_directory = path.join(cwd, 'config');
  await fs.promises.mkdir(download_directory, { recursive: true });

  if (source_url.protocol === 'file:') {
    await copy_files(source_url.pathname, urls, download_directory)
  }
  else {
    console.log("Start download");
    await download_files(source_url.href, urls, opts, download_directory);
  }

  console.log("Generate config");
  await mutate_config(cwd, `${download_directory}/config`);

  console.log("Generate progs");
  await write_progs(`${download_directory}/progs`);


  console.log("Done");
  console.log("Run 'npm install' to update subprojects in config/config");
}

async function mutate_config(root_path, config_path) {

  const content = await fs.promises.readFile(config_path, 'utf8');
  const lines = content.split('\n');

  const root_as_unix_path = path.resolve(root_path).replace(/\\/g, "/");
  const root_proj_path = `proj_path = "${root_as_unix_path}"`;

  // Detect if the configfile is registered. Exit if it is already registered
  if (lines.findIndex(line => line.startsWith(root_proj_path)) >= 0) {
    return;
  }

  // Find the last occurrence of "proj_path =" using reduce, 
  // or fallback to the first occurrence of "pvss_path =" if no match is found.
  const insert_at = lines.reduce(
    (acc, line, index) => line.search(/[ ]*proj_path[ ]*=/) > 0 ? index : acc,
    0
  ) || lines.findIndex(line => line.search(/[ ]*pvss_path[ ]*=/) >= 0);

  if (insert_at >= 0) {
    lines.splice(insert_at + 1, 0, root_proj_path);
  }

  // remove lines from the "level" files
  const remove_at = lines.findIndex(line => line.search(/##.*config\.level.*/) >= 0);
  if (remove_at >= 0) {
    lines.splice(remove_at, lines.length - remove_at);
  }

  const new_content = lines.join("\n");

  if (content !== new_content) {
    await fs.promises.writeFile(config_path, new_content, 'utf8');
  }
}


async function write_progs(progs_path) {
  await fs.promises.writeFile(progs_path, `version 1

auth "" ""
#Manager         | Start  | SecKill | Restart# | ResetMin | Options
WCCILpmon        | manual |      30 |        3 |        1 |
WCCOAui          | manual |      30 |        3 |        1 |PROD -m gedi -ssa
WCCOAui          | manual |      30 |        3 |        1 |LOCAL -m gedi -data 127.0.0.1 -event 127.0.0.1
WCCOAui          | manual |      30 |        3 |        1 |OFFLINE -n -m gedi
`, 'utf8');
}
