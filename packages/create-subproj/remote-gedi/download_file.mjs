import https from 'https';
import http from 'http';
import fs from 'fs';


export function download_file(url, options, destinationPath) {
  return new Promise((resolve, reject) => {
    const request = (url.protocol === "https:") ? https.get : http.get;

    request(url, options, (response) => {
      if (response.statusCode !== 200) {
        response.resume();
        if (response.statusCode === 404) {
          return resolve(); // skip this file, not an error
        }
        else if (response.statusCode === 401) { //no access
          return reject(new Error(`401`));
        }
        return reject(new Error(`Failed to download file: ${response.statusCode}`));
      }

      const file = fs.createWriteStream(destinationPath);
      response.pipe(file);
      file.on('finish', () => {
        file.close(() => {
          resolve();
        });
      });
    }).on('error', (err) => {
      reject(err);
    });
  });
}
