# create-subproj

* NPM: https://www.npmjs.com/package/create-subproj
* Source Code: https://gitlab.com/fholmer/subproj-workspace
* License: BSD License

## Summary

Create a subproj project

## Warning

* This is a beta version. Not ready for production.

## Usage

Open command line and run using npm:

```bash
$ cd /path/to/my/project
$ npm init subproj@latest -- --template https://tpl.example.internal/tpl.json
```

## Template format:

```json
{
    "git_origin_main": "https://ci.develop.internal/proj.git",
    "git_prod_main": "https://cd.prod.internal/proj.git",
    "npm_scope": "@subproj",
    "npm_registry": "https://ci.develop.internal/npm/",
    "npm_prod_registry": "https://cd.prod.internal/npm/"
}
```
