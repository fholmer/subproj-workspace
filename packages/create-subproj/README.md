# create-subproj

* NPM: https://www.npmjs.com/package/create-subproj
* Source Code: https://gitlab.com/fholmer/subproj-workspace
* License: BSD License

## Summary

Transform a project repository to a remote-gedi project

## Warning

* This is a beta version. Not ready for production.

## Usage

Open command line and run using npm:

```bash
$ cd /path/to/my/project
$ npm init subproj@latest -- --remote-gedi https://examplehost
```

## Example

If current environment is using self-signed certs:

```bash
# Optional, insecure
$ export GIT_SSL_NO_VERIFY=true
$ export NODE_TLS_REJECT_UNAUTHORIZED=0
```

Clone the production repo and transform it into a remote gedi

```bash
$ cd /opt/projects
$ git clone -o prod -b main https://gitea.examplehost/demo/demoproj
$ cd demoproj

$ npm init subproj@latest -- --remote-gedi https://examplehost
$ npm install

# Optional, connect a internal repo as origin
$ git remote add origin https://gitlab.internal/demo/demoproj

# Optional, connect internal npm package index
$ npm set @subproj:registry=https://gitlab.internal/api/v4/packages/npm/
$ npm set @subproj:prod:registry=gitea.examplehost/demo/-/packages/npm/
```
