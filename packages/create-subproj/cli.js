#!/usr/bin/env node

import { create_remote_gedi } from "./remote-gedi/create-remote-gedi.mjs";
import { create_from_template } from "./template/create-from-template.mjs";

async function main(args) {
    try {

        const insecure = (args.indexOf("--insecure") > 0);
        const login = (args.indexOf("--login") > 0);

        const ix_template = args.indexOf("-t") > 0
            ? args.indexOf("-t")
            : args.indexOf("--template");

        const ix_remote_gedi = args.indexOf("-g") > 0
            ? args.indexOf("-g")
            : args.indexOf("--remote-gedi");

        if (ix_template > 0) {

            const template = args[ix_template + 1] ?? "";
            if (template === "") {
                throw new Error("arg: -t, --template: no value");
            }
            await create_from_template(template, insecure, login);
        }
        else if (ix_remote_gedi > 0) {

            const remote_gedi = args[ix_remote_gedi + 1] ?? "";
            if (remote_gedi === "") {
                throw new Error("arg: -g, --remote-gedi: no value");
            }
            await create_remote_gedi(remote_gedi, insecure);
        }
        else {

            throw new Error("arg: -g, --remote-gedi is missing");
            //throw new Error("arg: -g, --remote-gedi, -t or --template is missing");
        }

    } catch (error) {
        console.error(error.message);
    }
}

main(process.argv);
