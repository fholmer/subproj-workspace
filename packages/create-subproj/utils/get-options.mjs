import fs from 'fs';
import { execSync } from 'child_process';
import readline from 'readline';
import path from 'path';
import os from 'os';


export async function get_url(url_or_path) {
    try {
        return new URL(url_or_path);
    }
    catch (err) {
        if (err.code === "ERR_INVALID_URL") {
            const filepath = await expand_path(url_or_path);
            return new URL(`file:///${filepath}`)
        }
        else {
            throw err;
        }
    }
}

export async function get_opts(insecure, login) {
    var opts = {};
    if (login) {
        opts = await interact_with_user();
    }
    if (insecure) {
        opts["rejectUnauthorized"] = false;
    }
    else {
        const npm_config = JSON.parse(
            execSync("npm config --json ls", { windowsHide: true })
                .toString("utf8")
        );
        const cafile = npm_config.cafile ?? null
        if (cafile) {
            opts["ca"] = fs.readFileSync(cafile);
        }
    }
    return opts;
}

async function expand_path(pth) {
    if (pth.startsWith('~')) {
        pth = path.join(os.homedir(), pth.slice(1));
    }

    const stats = await fs.promises.stat(pth);
    if (!(stats.isFile() || stats.isDirectory())) {
        throw new Error(`${pth} not found`);
    }
    return pth;
}

async function interact_with_user() {

    const rl = readline.promises.createInterface({
        input: process.stdin,
        output: process.stdout,
        terminal: true,
    });

    try {
        const username = await rl.question(`Login with username: `);

        //hide pass
        rl.input.on("keypress", function (c, k) {
            var len = rl.line.length;
            readline.moveCursor(rl.output, -len, 0);
            readline.clearLine(rl.output, 1);
            for (var i = 0; i < len; i++) {
                rl.output.write("*");
            }
        });

        const userpass = await rl.question("Password: ");

        if (username === "" && userpass === "") {
            return {};
        }

        const auth = Buffer.from(`${username}:${userpass}`).toString('base64');
        return {
            headers: {
                'Authorization': `Basic ${auth}`
            }
        }
    } catch (err) {
        throw err;
    } finally {
        rl.close();
    }
}