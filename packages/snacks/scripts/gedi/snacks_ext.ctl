// $License: NOLICENSE
//--------------------------------------------------------------------------------
/**
  @file $relPath
  @copyright $copyright
  @author frode
*/

//--------------------------------------------------------------------------------
// Libraries used (#uses)

//--------------------------------------------------------------------------------
// Variables and Constants

//--------------------------------------------------------------------------------
/**
*/
main()
{
  int id  = moduleAddMenu("Snacks-Sync");
  int id3 = moduleAddAction("Open Sync", "", "Ctrl+I", id, -1, "Sync");
  moduleAddDockModule("Snacks-Sync", "snacks/sync.pnl");
}

void Sync() {
  moduleAddDockModule("Snacks-Sync", "snacks/sync.pnl");
}
