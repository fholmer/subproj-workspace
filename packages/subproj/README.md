# subproj

* NPM: https://www.npmjs.com/package/subproj
* Source Code: https://gitlab.com/fholmer/subproj-workspace
* License: BSD License

## Summary

Handle subprojects on-prem.

## Warning

* This is a beta version. Not ready for production.

## Installation

Open command line and install using npm:

```bash
$ npm install subproj
```

## Usage

NPM scopes are a way of grouping related packages together, and also affect a few things about the way npm treats the package.

Use the same scope for all subprojects. e.g. `@subproj`

Scopes can be associated with a separate registry. This allows you to seamlessly use a mix of packages from the primary npm registry and one or more private registries, such as GitLab Packages or the open source Verdaccio project.

You can associate a scope with a private registry using npm config:

```bash
$ npm config set @subproj:registry https://gitlab.develop.internal/api/v4/packages/npm/
```

You should also add a separate registry for the `prod` environment:

```bash
$ npm config set @subproj:prod:registry https://gitlab.prod.internal/api/v4/packages/npm/
```

### Add scripts

Add a `postinstall` script that filters on the scope that you have chosen.

```bash
$ npm pkg set "scripts.postinstall=subproj postinstall @subproj"
```

Add a `push` script that uploads package dependencies to the `prod` environment

```bash
$ npm pkg set "scripts.push=subproj push @subproj"
```


### Install subprojects

Now you can install your subprojects

```bash
$ npm install @subproj/mylib1
$ npm install @subproj/mylib2
```

### Update project config

Finally you run the install command so that the post-install script updates `config/config` with the path of the subprojects

```bash
$ npm install
```

### Push to production

When its time to deploy you run the push command to upload all dependencies to the production environment

```bash
$ npm run push prod
```
