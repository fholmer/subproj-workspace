const npm_config = require('./utils/npm-config')
const gcm_auth = require('./utils/gcm-auth');

function log(...text) {
    console.log("subproj-login:", ...text);
}
function log_error(...text) {
    console.error("subproj-login:", ...text);
}

module.exports = (scope, environment) => {
    try {
        login(scope, environment);
    }
    catch (error) {
        if (error instanceof Error) {
            log_error(error.message);
        } else {
            log(error);
        }
    }
}

function login(scope, environment) {
    const desc = (environment || "")?`${scope} ${environment} environment`:`${scope}`;
    const config = npm_config.readConfig();
    const registry = npm_config.readRegistry(config, scope, environment);
    log(`Login npm to ${desc} registry ${registry.hostname}`);
    const auth_header = gcm_auth.getAuthHeader(registry);
    npm_config.writeAuthHeader(registry, auth_header);
    log(`login OK`);
}