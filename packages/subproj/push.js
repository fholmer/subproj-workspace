const fs = require('node:fs');
const path = require('node:path');

const npm_http = require('./utils/npm-request');
const npm_cache = require('./utils/npm-cache');
const npm_config = require('./utils/npm-config');
const pkg_metadata = require('./utils/pkg-metadata');
const gcm_auth = require('./utils/gcm-auth');

function log(...text) {
    console.log("subproj-push:", ...text);
}
function log_error(...text) {
    console.error("subproj-push:", ...text);
}

module.exports = (scope, environment) => {
    try {
        push_all(scope, environment);
    }
    catch (error) {
        if (error instanceof Error) {
            log_error(error.message);
        } else {
            log(error);
        }
    }
}

function push_all(scope, environment) {

    const config = npm_config.readConfig();
    const registry = npm_config.readRegistry(config, scope, environment);

    const desc = (environment || "") ? `${scope} ${environment} environment` : `${scope}`;
    log(`Push npm-packages to ${desc} registry ${registry.hostname}`);

    const auth_header = gcm_auth.getAuthHeader(registry);
    const cache_path = npm_config.readCaCachePath(config.cache);
    const pkg_lock = npm_config.readPackageLock();
    const pkg_list = npm_config.readPackageList();
    const ca_certs = npm_config.readCaCertsFile(config);

    pkg_list.forEach(pkg => {
        if (pkg.private === "true") {
            return;
        }
        if (!(pkg.name || null)) {
            return;
        }

        const location = pkg_lock.packages[pkg.location];

        if (!location.hasOwnProperty("integrity")) {
            log(`${pkg.name}@${pkg.version} : at ${pkg.realpath} : .tgz-file not found.`);
            return;
        }

        npm_http.httpRequest(npm_http.opts.dist_tags(registry, pkg, auth_header, ca_certs))
            .then((remote_version) => {

                if (JSON.parse(remote_version).latest === location.version) {
                    log(`${pkg.name}@${location.version} -> ${registry.hostname}: Package exists`);
                    return;
                }
                try {
                    const pkg_path = path.join(pkg.realpath, "package.json");
                    const tgz_path = npm_cache.integrityToPath(cache_path, location.integrity);
                    push_pkg(registry, auth_header, pkg_path, tgz_path, location.version, ca_certs);
                } catch (error) {
                    log_error(`${pkg.name}@${pkg.version} Error : ${error.message}`);
                }
            })
            .catch((error) => {
                log_error(`${pkg.name}@${pkg.version} -> ${registry.hostname} :: Error : ${error.message}`);
            });
    });
}

function push_pkg(registry, auth_header, pkg_path, tgz_path, lock_version, ca_certs) {

    const pkg_buffer = fs.readFileSync(pkg_path, { encoding: "utf8" });
    let pkg = JSON.parse(pkg_buffer);

    if (lock_version !== pkg.version) {
        throw new Error(`Version mismatch pkg-lock-file=${lock_version}`);
    }

    const tag = 'latest';
    const root = pkg_metadata.create(pkg, pkg_path, tgz_path, tag, registry);
    const data = JSON.stringify(root);
    const options = npm_http.opts.upload(registry, pkg, auth_header, data.length, ca_certs);

    log(`${pkg.name}@${pkg.version} : Uploading to ${registry.href}`);

    npm_http.httpRequest(options, data)
        .then((result) => {
            if (result === "") {
                log(`${pkg.name}@${pkg.version} : OK`);
            }
            else {
                log(`${pkg.name}@${pkg.version} : ${result}`);
            }
        })
        .catch((error) => {
            throw error;
        });
}
