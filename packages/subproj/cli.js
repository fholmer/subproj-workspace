#!/usr/bin/env node

function main(argv) {
    let command = argv.at(2) || "";
    let scope = argv.at(3) || "";
    if (scope == "") {
        scope = "@subproj"; //default scope
    }
    let environment = argv.at(4) || "";
    if (environment == "") {
        //environment = "prod"; //default env
    }

    switch (command) {
        case "init":
            command_init(scope, environment); break;

        case "postinstall":
            command_postinstall(scope, environment); break;

        case "push":
            command_push(scope, environment); break;

        case "login":
            command_login(scope, environment); break;

        default:
            console.log(`Command '${command}' not found`)
    }
}


function command_init(scope, environment) {
    init = require('./init.js');
    init(scope, environment);
}


function command_postinstall(scope, environment) {
    postinstall = require('./post-install.js');
    const package = require(`${process.cwd()}/package.json`);

    const pkgs = Object.keys(package["dependencies"]);
    postinstall(pkgs.filter(pkg => pkg.startsWith(`${scope}/`)), environment);
}


function command_push(scope, environment) {
    push_all = require('./push.js');
    push_all(scope, environment);
}

function command_login(scope, environment) {
    login = require('./login.js');
    login(scope, environment);
}
main(process.argv);
