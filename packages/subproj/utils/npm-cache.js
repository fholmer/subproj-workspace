const path = require('node:path');

module.exports.integrityToPath = integrityToPath;


function hashToSegments(hash) {
    // source :cacache/lib/util/hash-to-segments.js
    return [hash.slice(0, 2), hash.slice(2, 4), hash.slice(4)]
}
function contentDir(cache) {
    // source: cacache/lib/content/path.js
    const contentVer = 2;
    return path.join(cache, `content-v${contentVer}`)
}
function integrityToPath(cache, integrity) {
    // source: cacache/lib/content/path.js
    const [algorithm, base64Hash] = integrity.split("-");
    const sri = Buffer.from(base64Hash, 'base64');
    return path.join(
        contentDir(cache),
        algorithm,
        ...hashToSegments(sri.toString("hex"))
    )
}
