const { spawnSync } = require('child_process');

module.exports.getAuthHeader = getAuthHeader;

function getAuthHeader(register) {

    const trace = process.env["GCM_TRACE"] || null;

    const protocol = register.protocol.replace(/:$/, '');
    const hostname = register.host;  // host = hostname + port
    //const pathname = register.pathname.replace(/^\//, '');

    const ready = spawnSync("git", ["credential-manager", "--version"], { encoding: "utf8" });
    if (ready.status !== 0) {
        console.warn("Unable to login. GCM is not enabled.");
        return {};
    }

    const gcm = spawnSync("git", ["credential-manager", "get"], {
        input: `protocol=${protocol}\nhost=${hostname}\n\n`,
        encoding: "utf8"
    });
    if (trace) {
        console.log(gcm.status, gcm.stderr)
    }

    const pw = gcm.stdout.match(/^password=(.+)$/m);
    const usr = gcm.stdout.match(/^username=(.+)$/m);
    if (!pw || !usr) {
        throw new Error("Error authenticating with git-credential-manager");
    }

    if (gcm.status !== 0) {
        spawnSync("git", ["credential-manager", "erase"], {
            input: `protocol=${protocol}\nhost=${hostname}\nusername=${usr[1]}\npassword=${pw[1]}\n\n`,
            encoding: "utf8"
        });
        throw new Error(gcm.stderr);
    }

    spawnSync("git", ["credential-manager", "store"], {
        input: `protocol=${protocol}\nhost=${hostname}\nusername=${usr[1]}\npassword=${pw[1]}\n\n`,
        encoding: "utf8"
    });

    return {
        'Authorization': `Bearer ${pw[1]}`
    };
}