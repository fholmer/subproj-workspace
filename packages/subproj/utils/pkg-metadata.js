// # https://github.com/yarnpkg/yarn/blob/master/src/cli/commands/publish.js
// # https://github.com/yarnpkg/berry/blob/master/packages/plugin-npm/sources/npmPublishUtils.ts
// # https://github.com/npm/cli/blob/latest/workspaces/libnpmpublish/lib/publish.js
// 

const fs = require('node:fs');
const path = require('node:path');
const crypto = require('node:crypto');
const url = require('node:url');

module.exports.create = create_metadata;

function create_metadata(pkg, pkg_path, tgz_path, tag, registry) {
    const tgz_buffer = fs.readFileSync(tgz_path);

    const tbName = `${pkg.name}-${pkg.version}.tgz`;
    const tbURI = `${pkg.name}/-/${tbName}`;

    const root = {
        _id: pkg.name,
        access: null,
        name: pkg.name,
        description: pkg.description,
        'dist-tags': {
            [tag]: pkg.version,
        },
        versions: {
            [pkg.version]: pkg,
        },
        _attachments: {
            [tbName]: {
                content_type: 'application/octet-stream',
                data: tgz_buffer.toString('base64'),
                length: tgz_buffer.length,
            },
        },
    };

    const readme_path = path.join(path.dirname(pkg_path), "README.md");
    if (fs.existsSync(readme_path)) {
        const readme = fs.readFileSync(readme_path, { encoding: "utf8" });
        //root.readme = readme;
        pkg.readme = readme.replace(/\u00A0/g, '\u0020');
        pkg.readmeFilename = "README.md";
    }

    pkg._id = `${pkg.name}@${pkg.version}`;
    pkg.dist = pkg.dist || {};
    pkg.dist.shasum = crypto.createHash('sha1').update(tgz_buffer).digest('hex');
    pkg.dist.integrity = "sha512-" + crypto.createHash('sha512').update(tgz_buffer).digest('base64');
    pkg.dist.tarball = url.resolve(registry.href, tbURI).replace(/^https:\/\//, 'http://');

    return root;
}
