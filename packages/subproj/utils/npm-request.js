const https = require('node:https');
const http = require('node:http');
const url = require('node:url');

module.exports.httpRequest = httpRequest;
module.exports.opts = {
    dist_tags: opts_dist_tags,
    upload: opts_upload
}

function httpRequest(options, postData = null) {
    const request = (options.protocol === "https:") ? https.request : http.request;
    return new Promise((resolve, reject) => {
        const req = request(options, (res) => {
            let data = '';
            if (res.statusCode == 401) {
                reject(new Error(`${res.statusCode} - ${res.statusMessage}`))
            }
            res.on('data', (chunk) => {
                data += chunk;
            });

            res.on('end', () => {
                resolve(data);
            });
        });

        req.on('error', (error) => {
            reject(error);
        });

        if (postData) {
            req.write(postData);
        }

        req.end();
    });
}

function opts_dist_tags(registry, pkg, auth_header, ca) {
    return {
        protocol: registry.protocol,
        hostname: registry.hostname,
        port: registry.port,
        path: url.resolve(registry.pathname, `-/package/${pkg.name}/dist-tags`),
        method: 'GET',
        ca: ca,
        headers: { ...{}, ...auth_header },
    };
}

function opts_upload(registry, pkg, auth_header, content_length, ca) {
    return {
        protocol: registry.protocol,
        hostname: registry.hostname,
        port: registry.port,
        path: url.resolve(registry.pathname, pkg.name),
        method: 'PUT',
        ca: ca,
        headers: {
            ...{
                'Content-Type': 'application/json',
                'Content-Length': content_length,
                'Connection': 'keep-alive',
                'Npm-Command': 'publish'
            }, ...auth_header
        }
    };
}
