const path = require('node:path');
const fs = require('node:fs');
const { execSync } = require('node:child_process');


module.exports.readConfig = readConfig;
module.exports.readRegistry = readRegistry;
module.exports.readCaCachePath = readCaCachePath;
module.exports.readPackageLock = readPackageLock;
module.exports.readPackageList = readPackageList;
module.exports.writeAuthHeader = writeAuthHeader;
module.exports.readCaCertsFile = readCaCertsFile;

const proc_opt = {
    windowsHide: true
}

function readConfig() {
    return JSON.parse(execSync("npm config --json ls", proc_opt).toString("utf8"));
}

function readRegistry(config, scope, environment) {
    const env = (environment === "") ? "" : `:${environment}`;
    const registry_url = config[`${scope}${env}:registry`] || "";
    if (registry_url == "") {
        console.log(`\n!!! Register a '${env}'-registry by typing: npm set ${scope}${env}:registry=https://gitea.example.local/api/packages/subproj/npm/\n`);
        throw new Error(`Private registry not defined for scope: ${scope}${env}`);
    }
    return new URL(path.posix.join(registry_url, "/"));
}

function readCaCachePath(cache_path) {
    return path.join(cache_path, "_cacache");
}

function readPackageLock() {
    return JSON.parse(fs.readFileSync("./package-lock.json", { encoding: "utf8" }));
}

function readPackageList() {
    return JSON.parse(execSync("npm query .prod", proc_opt).toString("utf8"));
}

function readCaCertsFile(config) {
    const cafile = config.cafile ?? null
    if (cafile) {
        return fs.readFileSync(cafile)
    }
    return null;
}

function writeAuthHeader(registry, auth_header) {
    if (auth_header.hasOwnProperty("Authorization")) {
        if (auth_header["Authorization"].startsWith("Bearer ")) {
            const authToken = auth_header["Authorization"].split(" ")[1] || "";
            if (authToken !== "") {
                execSync(`npm config set //${registry.hostname}${registry.pathname}:_authToken=${authToken}`, proc_opt);
                //process.env[`NPM_CONFIG_//${registry.hostname}${registry.pathname}:_authToken`] = authToken;
                return;
            }
        }
    }
    throw new Error("Login failed");
}
