
const fs = require('node:fs');
const { execSync } = require('child_process');

module.exports = init;

function init(scope, env) {
    const opt = {
        windowsHide: true
    }

    if (!fs.existsSync('./post-install.js')) {
        console.log(`Set package.json:scripts.postinstall to 'subproj postinstall ${scope} ${env}'`);
        execSync(`npm pkg set "scripts.postinstall=subproj postinstall ${scope} ${env}"`, opt);
    }

    if (!fs.existsSync('./push.js')) {
        console.log(`Set package.json:scripts.push to 'subproj push ${scope} ${env}'`);
        execSync(`npm pkg set "scripts.push=subproj push ${scope} ${env}"`, opt);
    }
    if (!fs.existsSync('./login.js')) {
        console.log(`Set package.json:scripts.login to 'subproj login ${scope} ${env}'`);
        execSync(`npm pkg set "scripts.login=subproj login ${scope} ${env}"`, opt);
    }
    
    // if (!fs.existsSync('./.gitignore')) {
    //     fs.writeFileSync('./.gitignore', gitignore_template);
    // }
    // if (!fs.existsSync('./.gitattributes')) {
    //     fs.writeFileSync('./.gitattributes', gitattributes_template);
    // }
}

// const gitignore_template = `bin
// db
// log
// cache
// colorDB/.colorDB.lock
// panels/**/*.ba?
// node_modules
// config/config
// config/config.level
// config/progs
// config/influxdb.conf
// *.pem
// *.der
// *.crt
// config/*.key
// data/iec104
// data/opcua
// data/Reporting
// data/xls_report
// `
//
// const gitattributes_template = `* text=auto
// *.pnl -text
// *.pnl -merge
// *.ctc -diff
// `
