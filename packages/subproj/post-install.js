const fs = require('node:fs');
const path = require('node:path');
const package = require('./package.json');

function post_install(pkgs, environment) {

    if (fs.existsSync('./config/config')) {
        update_config_file(pkgs.filter(pkg => pkg !== package["name"]));
    }
    else {
        console.log("config/config not found");
    }

}
module.exports = post_install;


function update_config_file(pkgs) {

    fs.readFile('./config/config', 'utf8', (err, data) => {

        if (err) {
            console.error(err);
            return;
        }

        const pkg_filter = new RegExp(`[# ]*proj_path[ ]*=[ ]*".*node_modules/(${pkgs.join("|")})"`);
        const content = data.toString();

        // lines: config_file as a list, but without lines referencing node_modules.
        const lines = content
            .split("\n")
            .filter(line => !pkg_filter.test(line));

        const insert_at = lines.findIndex(line => {
            return line.search(/[ ]*pvss_path[ ]*=/) >= 0;
        });
        if (insert_at < 0) {
            console.error("error: pvss_path not found");
            return;
        }

        const root = path.resolve(".").replace(/\\/g, "/");
        const pkg_paths = pkgs.map((line) => `proj_path = "${root}/node_modules/${line}"`);

        // discover subprojects in node_modules and insert them to new config_file
        lines.splice(insert_at + 1, 0, ...pkg_paths);

        const new_content = lines.join("\n");

        if (content !== new_content) {
            console.log("Updating config/config");

            fs.writeFile('./config/config', new_content, err => {
                if (err) {
                    console.error(err);
                } else {
                    // file written successfully
                }
            });
        }

        lines.forEach(line => {
            if (line.search(/[# ]*(pvss_path|proj_path)[ ]*=/) >= 0) {
                console.log(line);
            }
        });
    });
}

